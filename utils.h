#ifndef UTILS_H
#define UTILS_H

#include <stdbool.h>

/* safe_* functions operate as their non-safe counterparts, except instead of
 * setting errno, print an error and call their callback function.
 */
unsigned safe_strtoui (char *, char **, int, char *, void (*)(void));
long unsigned safe_strtoul (char *, char **, int, char *, void (*)(void));
long long unsigned safe_strtoull (char *, char **, int, char *, void (*)(void));
long double safe_strtold (char *, char **, int, char *, void(*)(void));

/* Print errno error message and exit with errorlevel */
__attribute__((cold, noreturn))
void fail (const char *restrict const);

/* Print custom message and exit with errorlevel */
__attribute__((cold, noreturn))
void die (const char *restrict const, ...);

/* debug output system */
extern int (*debug)(const char *restrict const, ...);
void enable_debug (void);
bool debug_enabled (void);

#endif /* UTILS_H */
